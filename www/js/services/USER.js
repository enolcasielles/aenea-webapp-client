    servicios.factory('USER',function(API) {

        var user = {};

        return {

            /**
             **Establece los datos del usuario
             */
            setUser : function(usuario) {
                user.id = usuario._id;
                user.email = usuario.email;
                user.nombre = usuario.nombre;
                user.imagen = usuario.imagen;
                user.social = usuario.social;
                user.preferencias = usuario.preferencias;
                user.tarjeta = usuario.tarjeta;
                user.actividad = usuario.actividad;
            },


            /**
             ** Actualiza los datos que haya en este momento del usario en el servidor
             ** Devuelve true si se ha actualizado correctamente o false si ocurre algun problema
             */
            updateUserDataServer : function() {
                API.updateUser(user).success(function (data) {
                    return true;
                }).error(function (error) {
                    return false;
                });
            },

            /**
             ** Devuelve el nombre del usuario
             */
            getNombre : function() {
                return user.nombre;
            },

            /**
             ** Devuelve el id del usuario
             */
            getId : function() {
                return user.id;
            },

            /**
             ** Devuelve el email del usuario
             */
            getEmail : function() {
                return user.email;
            },

            /**
             ** Devuelve la url con iamgen
             */
            getImagen : function() {
                return user.imagen;
            },

            /**
             ** Devuelve las preferencias del usuario
             */
            getPreferencias : function() {
                return user.preferencias;
            },

            /**
             ** Devuelve la red por la que el usuario se registra
             */
            getSocial : function() {
                return user.social;
            },


            /**
             ** True si el usuario ha registrado tarjeta o false si no
             */
            tieneTarjeta : function() {
                if(user.tarjeta == 'SI') return true;
                return false;
            },

            /**
             ** Establece si el usuario ha registrado tarjeta o no
             ** @param bol (Boolean) true si ha registrado tarjeta o false si no
             */
            setTieneTarjeta : function(bol) {
                if (bol) user.tarjeta = "SI";
                else user.tarjeta = "NO";
            },

            /**
             ** Devuelve la actividad del usuario
             */
            getActividad : function() {
                return user.actividad;
            },


            /**
             ** Registra una nueva actividad 
             ** @param tipo (String) El tipo de la actividad (CREATE, SHAREFACEBOOK, DONATE,...)
             ** @param idProyecto (Number) Un entero que represente al proyecto asociado a esta actividad
             ** @param fecha (Date) Representa la fecha en la que ocurre
             */
            addActividad : function(tipo,idProyecto,fecha) {
                user.actividad.push({
                    tipo : tipo,
                    proyectoAsociado : idProyecto,
                    fecha : fecha
                });
            },


            /**
             ** Registra una actividad a partir del objeto que recibe
             ** @param obj (Objeto) el objeto que almacena los datos de la actividad
             */
            addActividadObjeto : function(obj) {
                user.actividad.push(obj);
            }


        }

    });