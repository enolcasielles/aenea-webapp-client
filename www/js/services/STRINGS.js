    //Factory que almacena los distintos Strings de la App en los distintos idiomas, ingles y español
    servicios.factory('STRINGS',function() {

        //Constante que define el numero de idiomas
        var NUM_IDIOMAS = 2;

        return {

            //Constantes que definen los distintos idiomas
            ES : 0, 
            EN : 1,

            //Variable para definir el idioma en el que se mostrara la app
            idioma : this.EN,  //Por defecto ingles

            //Metodo para configurar el idioma
            setIdioma : function(idioma) {
                if(idioma >= NUM_IDIOMAS || idioma < 0) idioma = this.EN;  //Si es un valor no valido cogemos ingles
                else this.idioma = idioma;
            },

            
            //Definir aqui todos los strings que se quiera utiliza en la App como un array en el 
            //que cada elemento se corresponde a un idioma en el mismo orden que se declaran los idiomas
            tituloLogin : ['Inicio','Login'],


        }

    });
