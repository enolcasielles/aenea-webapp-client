
var directivas = angular.module('aenea.directives', []);

directivas.directive('ionSearch', function() {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                getData: '&source',
                model: '=?',
                search: '=?filter'
            },
            link: function(scope, element, attrs) {
                attrs.minLength = attrs.minLength || 0;
                scope.placeholder = attrs.placeholder || '';
                scope.search = {value: ''};

                if (attrs.class)
                    element.addClass(attrs.class);

                if (attrs.source) {
                    scope.$watch('search.value', function (newValue, oldValue) {
                        if (newValue.length > attrs.minLength) {
                            scope.getData({str: newValue}).then(function (results) {
                                scope.model = results;
                            });
                        } else {
                            scope.model = [];
                        }
                    });
                }

                scope.clearSearch = function() {
                    scope.search.value = '';
                };
            },
            template: '<div class="item-input-wrapper">' +
                        '<i class="icon ion-android-search"></i>' +
                        '<input type="search" placeholder="{{placeholder}}" ng-model="search.value">' +
                        '<i ng-if="search.value.length > 0" ng-click="clearSearch()" class="icon ion-close"></i>' +
                      '</div>'
        };
});

directivas.directive('map', function() {
  return {
    restrict: 'E',
    scope: {
      onCreate: '&'
    },
    link: function ($scope, $element, $attr) {
      function initialize() {
        var mapOptions = {
          center: new google.maps.LatLng(43.07493, -89.381388),
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.TERRAIN,
          draggable : false,
          disableDefaultUI : true
        };
        var map = new google.maps.Map($element[0], mapOptions);
  
        $scope.onCreate({map: map});

        // Stop the side bar from dragging when mousedown/tapdown on the map
        google.maps.event.addDomListener($element[0], 'mousedown', function (e) {
          e.preventDefault();
          return false;
        });
      }

      if (document.readyState === "complete") {
        initialize();
      } else {
        google.maps.event.addDomListener(window, 'load', initialize);
      }
    }
  }
});
directivas.directive("moveNextOnMaxlength", function() {
    return {
        restrict: "A",
        link: function($scope, element) {
            element.on("input", function(e) {
                if(element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.next();
                    if($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});




/**
 **Directiva que define la animacion para el menu
 */
directivas.directive('slideAlong', function($timeout, $ionicSideMenuDelegate) {
  return  {
    link: function($scope, $element, $attrs) {
      $scope.$watch(function() {
        return $ionicSideMenuDelegate.getOpenRatio();
      }, function(ratio) {

        // retrieve the offset value from the offset attribute
        var offset = parseInt($attrs.offset);

        // set the new position
        var position = $attrs.side == 'left' ? (ratio * (offset * -1)) + (offset) : (ratio * (offset * -1) - offset);

        // we want to set the transition to 500ms (arbitrary) when 
        // clicking/tapping and 0ms when swiping
        $element[0].style.webkitTransition = (ratio === 0 || ratio === 1 || ratio === -1) ? '200ms' : '0ms';

        // we set the offset according to the current ratio
        $element[0].style.webkitTransform = 'translate3d(' + position + '%, 0, 0)';

      });
    }
  };
});


