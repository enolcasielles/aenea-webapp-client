var aenea = angular.module('aenea', ['ionic', 'aenea.controllers', 'aenea.services', 'aenea.directives', 'ngCordova', 'ionic.contrib.frost','akoenig.deckgrid'])
    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {

         if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
         }
         if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
         }
        });
    })
    .config(function ($stateProvider, $urlRouterProvider, $cordovaFacebookProvider) {

        $stateProvider
            .state('inicio', {
                url:"/inicio",
                templateUrl: 'templates/inicio.html',
                controller: 'InicioCtrl'
            })

            .state('eventmenu', {
                url: "/event",
                abstract: true,
                templateUrl: "templates/event-menu.html"
            })
    
            .state('eventmenu.home', {
                url: "/home",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/home.html"
                    }
                }
            })

            .state('proyectos', {
                url:"/proyectos/:nombre",
                templateUrl : 'templates/proyectos.html',
                controller : 'ProyectosCtrl'
            })

            .state('proyecto', {
                url:"/proyecto/:id",
                templateUrl : 'templates/proyecto.html',
                controller : 'ProyectoDetailCtrl'
            })
            
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })

            .state('registra', {
                url: '/registra',
                templateUrl: 'templates/register.html',
                controller: 'RegisterCtrl'
            })

            .state('perfil', {
                cache:false,
                url: '/perfil',
                templateUrl: 'templates/perfil.html',
                controller: 'PerfilCtrl'
            })

        $urlRouterProvider.otherwise('/inicio');
    });