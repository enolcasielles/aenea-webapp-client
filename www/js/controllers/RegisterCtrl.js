
controladores.controller('RegisterCtrl', function ($rootScope, $scope, API, USER, $window) {
    
    //Datos a enviar para el registro, algunos los modificara el formulario, otros son los datos por defecto
    $scope.user = {
        email: "",
        password: "",
        nombre: "",
        social : "NO",
        preferencias : {},
        imagen : "",
        actividad : [],
        tarjeta : "NO"
    };

    //Metodo para llevar a cabo el registro
    $scope.createUser = function () {
        var email = this.user.email;
        var password = this.user.password;
        var uName = this.user.nombre;
        
        //Compruebo que se hayan introducido datos
        if(!email || !password || !uName) {
            $rootScope.notify("Datos no válidos",999);
            return false;
        }
        $rootScope.show('Registrando usuario...');
        
        //Registro una primera actividad de tipo CREATE si no esta ya registrada
        if (this.user.actividad.length == 0) {
            var actividad = {
                tipo : "CREATE",
                proyecto : -1,   //No esta asociada a ningun proyecto
                fecha : new Date()  //La fecha actual
            };
            this.user.actividad.push(actividad);
        }

        //Llamo a la API para procesar el registro
        API.signup(this.user).success(function (data) {
            console.log(data);  //Depurar si llegan bien los datos
            USER.setUser(data);  //Iniciamos USER
            $rootScope.setToken(email);  //Almaceno el token para crear sesion
            $rootScope.hide();
            $window.location.href = ('#/proyectos/0');
        }).error(function (error) {
            $rootScope.hide();
            if(error && error.error && error.error.code == 11000)
            {
                $rootScope.notify("Error. Ya existe un usuario con este correo",1500);
            }
            else
            {
                $rootScope.notify("Vaya...Se ha producido algun error. Inténtalo de nuevo",1500);
            }
            
        });
    }

    $scope.backInit = function() {
        $window.location.href = ('#/inicio');
    }
});