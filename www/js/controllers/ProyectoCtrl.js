controladores.controller('ProyectoDetailCtrl', function ($rootScope,$scope, $ionicSlideBoxDelegate,  API, PROYECTOS, $ionicModal, $ionicPopup, $window, $stateParams, SHARE, USER) {

    $scope.proyecto = PROYECTOS.get($stateParams.id);


    //Separo en parrafos la descripcion
    $scope.proyecto.parrafos = $scope.proyecto.descripcion.split("<br/>");

    //Calculo los dias que restan para finalizar a partir de su fecha de finalizacion
    var oneDay = 24*60*60*1000; 
    var firstDate = new Date();
    var fechaFinaliza = $scope.proyecto.fechaFinaliza.split("-");
    console.log(fechaFinaliza);
    var secondDate = new Date(parseInt(fechaFinaliza[0]),parseInt(fechaFinaliza[1])-1,parseInt(fechaFinaliza[2]));

    $scope.proyecto.dias = Math.round((secondDate.getTime() - firstDate.getTime())/(oneDay));

    //Calculo los proyectos similares
    $scope.proyecto.similares = PROYECTOS.calculaSimilares($scope.proyecto);

    /**
     ** Metodo para volver al listado de proyectos
     ** @param filtrar (Boolean) Indica si se vuelve al conjunto de proyectos total o filtrado por ong
     */
    $scope.backProyectos = function(filtrar) {
        if (filtrar) $window.location.href = ('#/proyectos/0'); //Un cero como parametro para indicar que se carguen todos
        else $window.location.href = ('#/proyectos/'+$scope.proyecto.nombre);
    }

    /**
     ** Metodo para acceder a otro proyecto
     ** @param proyecto (Objeto) Objeto Proyecto al que se quiere acceder
     */
    $scope.goProyecto = function(proyecto) {
        window.location.href = ('#/proyecto/'+proyecto._id);
    }

    
    //Modal para donar
    $ionicModal.fromTemplateUrl('templates/donateModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.modal = modal
    });  
    
    $scope.openModal = function() {
        $scope.modal.show();
    };
  
    $scope.closeModal = function() {
        $scope.modal.hide();
    };


    //Modal para mostrar la descripcion
    $ionicModal.fromTemplateUrl('templates/descripcionModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.descripcionModal = modal
    });  
    
    $scope.openDescripcionModal = function() {
        $scope.descripcionModal.show();
    };
  
    $scope.closeDescripcionModal = function() {
        $scope.descripcionModal.hide();
    };

    $scope.$on('$destroy', function() {
        $scope.modal.remove();
        $scope.descripcionModal.remove();
    });


    //Modal para escoger como compartir
    $ionicModal.fromTemplateUrl('templates/shareModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.shareModal = modal
    });  
    
    $scope.openShareModal = function() {
        $scope.shareModal.show();
    };
  
    $scope.closeShareModal = function() {
        $scope.shareModal.hide();
    };   


    //Modal para mostrar el metodo de pago
    $scope.mascara = false;
    $ionicModal.fromTemplateUrl('templates/donateModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.donateModal = modal
        console.log(modal);
    });  
    
    $scope.openDonateModal = function() {
        $scope.mascara = true;
        $scope.donateModal.show();
    };
  
    $scope.closeDonateModal = function() {
        $scope.donateModal.hide();
    };  

    //Metodo para compartir el proyecto. Pasar como parametro la via
    $scope.shareBy = function(social) {
        
        SHARE.setDatos($scope.proyecto.nombre,'',$scope.proyecto.descripcion);

        var tipo = "",
            idProyecto = $scope.proyecto._id,
            fecha = new Date();
        
        if (social == 'facebook') {
            SHARE.facebookShare();
            tipo = "SHARE_FACEBOOK";
        }

        else if (social == 'twitter') {
            SHARE.twitterShare();
            tipo = "SHARE_TWITTER";
        }

        else if(social == 'whatsapp') {
            SHARE.whatsappShare();
            tipo = "SHARE_WHATSAPP";
        }

        else if(social == 'mail') {
            SHARE.mailShare();
            tipo = "SHARE_MAIL";
        }

        else if(social == 'pinterest') {
            SHARE.pinterestShare();
            tipo = "SHARE_PINTEREST";
        }

        else {
            SHARE.moreShare();
            tipo = "SHARE_OTHER";
        }

        //Agrego la actividad
        USER.addActividad(tipo,idProyecto,fecha);

        //Actualizo en el servidor
        USER.updateUserDataServer();

    };


    //Metodo para crear el mapa
    $scope.mapCreated = function(map) {
        $scope.map = map;
        $scope.map.setCenter(new google.maps.LatLng($scope.proyecto.latitud, $scope.proyecto.longitud));
    };



    //Objeto con la estrucutura para registrar la donacion
    $scope.donacion = {
        proyecto : 0.20,
        aenea : 0.01,
        total : 0.21
    };

    //Objeto con la estructura para registrar la tarjeta
    $scope.card = {
        numero : {
            parte1 : undefined,
            parte2 : undefined,
            parte3 : undefined,
            parte4 : undefined,
        },
        mes : undefined,
        anyo : undefined,
        cvc : undefined
    };

    //Inicio la trjeta si la tenia registrada
    if ($rootScope.getTarjetaLocalStorage() != null) {  //El usuario tiene tarjeta registrada
        $scope.card = $rootScope.getTarjetaLocalStorage();
        $scope.tieneTarjeta = true; 
        console.log("Tiene tarjeta");
    }
    else {
        $scope.tieneTarjeta = false;
        console.log("No tiene tarjeta");
    }


    //Pop up para configurar la donacion y confirmarla
    $scope.showPopup = function() {

        $ionicPopup.show({
            templateUrl: 'templates/donateModal.html',
            title: 'Tu donación',
            scope: $scope,
            buttons: [
                { text: 'Cancelar' },
                {
                    text: '<b>Confirmar</b>',
                    type: 'button-balanced',
                    onTap: function(e) {
                        //Ha pulsado confirmar
                        //Comprobamos que los numeros esten correctos
                        var parte1 = $scope.card.numero.parte1;
                        var parte2 = $scope.card.numero.parte2;
                        var parte3 = $scope.card.numero.parte3;
                        var parte4 = $scope.card.numero.parte4;
                        if (parte1 == undefined || parte2 == undefined || parte3 == undefined || parte4 == undefined) {
                            $rootScope.notify("Numero de tarjeta no válido",1000);
                            return;
                        }

                        if (parte1.toString().length != 4 || parte2.toString().length != 4 || parte3.toString().length != 4 || parte4.toString().length != 4) {
                            $rootScope.notify("Numero de tarjeta no válido",1000);
                            return;
                        }

                        if(parseInt(parte1) == NaN || parseInt(parte2) == NaN || parseInt(parte3) == NaN || parseInt(parte4) == NaN) {
                            $rootScope.notify("Numero de tarjeta no válido",1000);
                            return;
                        }

                        //Comprobamos el mes y el año
                        var mes = $scope.card.mes;
                        var anyo = $scope.card.anyo;
                        if (mes == undefined || anyo == undefined) {
                            $rootScope.notify("Fecha incorrecta",1000);
                            return;
                        }

                        if(parseInt(mes) > 12 || parseInt(mes) == NaN || anyo.toString().length != 2 || parseInt(anyo) == NaN) {
                            $rootScope.notify("Fecha incorrecta",1000);
                            return;
                        }

                        //Comrobamos el cvc
                        var cvc = $scope.card.cvc;
                        if (cvc == undefined) {
                            $rootScope.notify("Código incorrecto",1000);
                            return;
                        }

                        if(cvc.toString().length != 3 || parseInt(cvc) == NaN) {
                            $rootScope.notify("Código incorrecto",1000);
                            return;
                        }

                        //Toto esta ok, anotamos la tarjeta en el local storage sin el cvc
                        $scope.card.cvc = "";
                        $rootScope.setTarjetaLocalStorage($scope.card);

                        $rootScope.notify("Gracias por colaborar!",1500);

                        //Actualizamos datos del usuario (Tarjeta : SI y añadimos actividad)
                        USER.setTieneTarjeta(true);
                        USER.addActividadObjeto({
                            tipo : "DONATE",
                            cantidadAenea : $scope.donacion.aenea,
                            cantidadProyecto : $scope.donacion.proyecto, 
                            cantidadTotal : $scope.donacion.total,
                            proyectoAsociado : $scope.proyecto._id,
                            fecha : new Date()
                        })

                        //Actualizmaos usuario en el servidor
                        USER.updateUserDataServer();

                        //Actualizamos datos proyecto
                        $scope.proyecto.capitalRecaudado += $scope.donacion.proyecto;
                        $scope.proyecto.numeroDonantes++;

                        //Actualizamos datos del proyecto en el servidor
                        API.updateProject($rootScope.getToken(), $scope.proyecto._id, $scope.proyecto.capitalRecaudado, $scope.proyecto.numeroDonantes++ );

                    }
                }
            ]
        }).then(function(res) {
            
        });

    };


    $scope.clickGo21 = function() {
        $scope.donacion.aenea += 0.01;
        $scope.donacion.proyecto += 0.20;
        $scope.donacion.total = ($scope.donacion.aenea + $scope.donacion.proyecto).toFixed(2);
    }

    $scope.clickMore20 = function() {
        $scope.donacion.proyecto += 0.20;
        $scope.donacion.total = ($scope.donacion.aenea + $scope.donacion.proyecto).toFixed(2);
    }


    //Metodo para registrar un pago
    $scope.pagoCon = function(metodo) {
        if (metodo == 'tarjeta') {
            console.log($ionicSlideBoxDelegate);
            $ionicSlideBoxDelegate._instances[3].next(200);  // TODO Corregir esto!!
        }
        else {
            //Registro en el servidor el intento de configurar este pago
            USER.addActividadObjeto({
                tipo : "CONFIGURA_METODO_PAGO",
                metodo : metodo,
                proyectoAsociado : $scope.proyecto._id,
                fecha : new Date()
            })

            //Actualizmaos usuario en el servidor
            USER.updateUserDataServer();

            //Informo al usuario de que el método aún no está disponible
            $rootScope.notify("Metodo de pago aun no disponible",1500);
        }
    }


});