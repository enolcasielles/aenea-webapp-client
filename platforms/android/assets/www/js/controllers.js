//Instancio un modulo sobre el que se generaran los controladores
var controladores = angular.module('aenea.controllers', ['aenea.services']);/*
 *Controlador para manejar el estado inicio, que implementa la funcionalidad para iniciar
 *sesion con facebook o twitter. 
 */
controladores.controller('InicioCtrl', function ($state, $rootScope, $scope, $window, API, USER, $cordovaOauth, $http){
    

    //Si el usuario ya tiene sesion iniciada accedemos a la app
    if ($rootScope.isSessionActive()) {
        //Recupero datos del usuario
        var email = $rootScope.getToken();
        API.getUser(email).success(function (data) {
           USER.setUser(data);
           $window.location.href = ('#/proyectos/0'); 
        }).error(function (error) {
            $rootScope.notify("Error en el servidor. Imposible obtener datos",1999);
        });
    }


    //Metodo para iniciar con facebook
    $scope.facebookLogin = function() {
        $cordovaOauth.facebook("276563885847575",["email"]).then(function(result) {
            //Recupero datos que preciso del usuario
            var access_token = result.access_token;
            $http.get("https://graph.facebook.com/v2.3/me", { params: { access_token: result.access_token, fields: "id,name,email,picture", format: "json" }}).then(function(result) {
                $rootScope.show("Iniciando...");
                var usuario = {
                    email: result.data.email,
                    password: "",
                    nombre: result.data.name,
                    social: "FACEBOOK",
                    preferencias : {},
                    imagen : result.data.picture.data.url,
                    actividad : [],
                    tarjeta : "NO"
                };
                usuario.actividad.push({
                    tipo : "CREATE",
                    proyecto : -1,   //No esta asociada a ningun proyecto
                    fecha : new Date()  //La fecha actual
                });
                API.signup(usuario).success(function(data) {
                    //El servidor me devolvera la misma estructura de datos si era el primer registro o la 
                    //que hubiese almacenada si no era un nuevo usuario. Sea como sea almaceno la que me envia
                    USER.setUser(data);
                    $rootScope.setToken(data.email);
                    $rootScope.setFacebookToken(access_token);
                    $rootScope.hide();
                    $window.location.href = ('#/proyectos/0');
                });
            }, function(error) {
                $rootScope.notify("Algun error se produjo, inténtalo de nuevo",999);
                console.log(error);
            });
            
        }, function(error) {
            $rootScope.notify("Algun error se produjo, inténtalo de nuevo",999);
            console.log(error);
        });
    }
    

});
/*
 *Controlador para manejar el estado Login, define un objeto user en el scope con los campos email y password
 *Ademas define un metodo en el scope para validar el usuario. 
 */
controladores.controller('LoginCtrl', function ($rootScope, $scope, API, USER, $window) {

    //Datos que se precisa para validar el login
    $scope.user = {
        email: "",
        password: ""
    };


    //Metodo para validar el usuario
    $scope.validateUser = function () {
        var email = this.user.email;
        var password = this.user.password;
        if(!email || !password) {
            $rootScope.notify("Error. Datos no válidos",999);
            return false;
        }
        $rootScope.show('Comprobando datos...');
        API.signin($scope.user).success(function (data) {
            USER.setUser(data);  //Iniciamos USER
            $rootScope.setToken(email); 
            $rootScope.hide();
            $window.location.href = ('#/proyectos/0');
        }).error(function (error) {
            console.log(error);
            $rootScope.hide();
            if (error && error.code == 1) {  //Email no registrado
                $rootScope.notify("Error. Correo no registrado",1500);
            }
            else if (error && error.code == 2) {  //Contraseña incorrecta
                $rootScope.notify("Error. Contraseña incorrecta",1500);
            }
            else {
                $rootScope.notify("Vaya...Se ha producido algun error. Inténtalo de nuevo",1500);
            }
        });
    }

    //Metodo para volver a inicio
    $scope.backInit = function() {
        $window.location.href = ('#/inicio');
    }

});controladores.controller('ProyectosCtrl', function ($rootScope, $scope, $ionicHistory, $stateParams, API, USER, $ionicScrollDelegate, $ionicModal, $window, PROYECTOS, $ionicSideMenuDelegate) {


    $rootScope.$on('fetchAll', function(){
        
        $scope.ramas = PROYECTOS.getRamas();

        $scope.ramaSeleccionada = '';


        $rootScope.show("Obteniendo...");
        API.getAll($rootScope.getToken()).success(function (data, status, headers, config) {
            $scope.proyectos = [];

            var ong = $stateParams.nombre;
            var filtrar = ong != '0' ? true : false;

            console.log("Ong: " + ong + " ; filtrar: " + filtrar);

            for (var i=0 ; i<data.length ; i++) {

                if (filtrar && ong != data.ong) {
                    continue;
                }

                $scope.proyectos.push(data[i]);

                //Url de la imagen principal
                $scope.proyectos[i].imagen = API.getBase() + $scope.proyectos[i].imagen;

                //Url de la imagen de la ong
                $scope.proyectos[i].imagenOng = API.getBase() + $scope.proyectos[i].imagenOng;

                //Url de las imagenes del slider
                $scope.proyectos[i].imagenesSlider = $scope.proyectos[i].imagenesSlider.split(";");
                for (var j=0 ; j<$scope.proyectos[i].imagenesSlider.length ; j++) {
                    $scope.proyectos[i].imagenesSlider[j] = API.getBase() +  $scope.proyectos[i].imagenesSlider[j];
                }
                
                //Almaceno las ramas a la que pertenece
                $scope.proyectos[i].ramas = $scope.proyectos[i].rama.split(";");

            };
            if($scope.proyectos.length == 0) {
                $scope.noData = true;
            }
            else
            {
                $scope.noData = false;
                PROYECTOS.setProyectos($scope.proyectos);
            }

            //Añado al scope la informaion del usuario para formar el menu
            $scope.usuario = {};
            $scope.usuario.imagen =  "img/no-photo.png";
            if ($rootScope.getProfileImage()) $scope.usuario.imagen = $rootScope.getProfileImage();
            else if (USER.getImagen() != "") $scope.usuario.imagen = USER.getImagen();
            $scope.usuario.nombre = USER.getNombre();

            $rootScope.hide();
        }).error(function (data, status, headers, config) {
            $rootScope.hide();
            $rootScope.notify("Ocurrio un problema al recuperar los datos. Intentelo de nuevo mas tarde");
        });

    });

    $rootScope.$broadcast('fetchAll');


    //Variable que controla la visibilidad del buscador
    $scope.showBuscador = false;

    //Metodo para ocultar y/o mostrar el buscador
    $scope.cambiaVisibilidadBuscador = function() {
        if (this.showBuscador) {  //Si estoy cerrando el buscador muestro todos
            $scope.proyectos = PROYECTOS.muestraTodos();
            this.keyBuscar = '';  //Reinicio la palabra a buscar
        }
        else {  //Si estoy abriendo el buscador
            //Limpio el filtro por si habia alguno activo
            $scope.ramaSeleccionada = '';
        }
        this.showBuscador = !this.showBuscador;  //Cambio el estado
    }

    //Metodo para buscar en los proyectos
    $scope.buscar = function() {
        if(this.keyBuscar.length < 3) return;
        $scope.proyectos = PROYECTOS.buscar(this.keyBuscar);
    }


    //Añado un metodo para sacar el menu
    $scope.sacaMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.filtra = function(rama) {

        if (rama == $scope.ramaSeleccionada) {  //Muestro todos
            $scope.proyectos = PROYECTOS.quitaFiltros();
            $scope.ramaSeleccionada = '';
            console.log('He pinchao en la seleccionada');
        }
        else {
            $scope.proyectos = PROYECTOS.filtra(rama);
            $scope.ramaSeleccionada = rama;
            
        }

        //Scroll top
        $ionicScrollDelegate.scrollTop();
    };

    $scope.goProyecto = function(id) {
        $window.location.href = ('#/proyecto/'+id);
    };

    $scope.goPerfil = function() {
        $window.location.href = ('#/perfil');
        $ionicSideMenuDelegate.toggleLeft(false);  //Oculto el menu
        $ionicHistory.clearCache();  //Limpio cache para que se recarguen los datos
        $ionicHistory.clearHistory();
    };

    //Metodo para ordenar el conjunto de proyectos. Se llamara cuando se pulse uno de los botones del menu
    $scope.ordena = function(filtro) {
        
        $ionicSideMenuDelegate.toggleLeft(false);  //Oculto el menu
        $ionicScrollDelegate.scrollTop();
        $scope.ramaSeleccionada = '';  //Quito el posible filtro
        
        if (filtro == 'recientes') {
            $scope.proyectos = PROYECTOS.ordenaReciente();
        }

        else if(filtro == 'finaliza') {
            $scope.proyectos = PROYECTOS.ordenaFechaFinaliza();
        }

        else if(filtro == 'completado') {
            $scope.proyectos = PROYECTOS.ordenaCompletado();
        }

        else if(filtro == 'donaciones') {
            $scope.proyectos = PROYECTOS.ordenaNumDonantes();
        }

        else {
            //Muestro todos
            $scope.proyectos = PROYECTOS.muestraTodos();
        }
    }


});
controladores.controller('ProyectoDetailCtrl', function ($rootScope,$scope, $ionicSlideBoxDelegate,  API, PROYECTOS, $ionicModal, $ionicPopup, $window, $stateParams, SHARE, USER) {

    $scope.proyecto = PROYECTOS.get($stateParams.id);


    //Separo en parrafos la descripcion
    $scope.proyecto.parrafos = $scope.proyecto.descripcion.split("<br/>");

    //Calculo los dias que restan para finalizar a partir de su fecha de finalizacion
    var oneDay = 24*60*60*1000; 
    var firstDate = new Date();
    var fechaFinaliza = $scope.proyecto.fechaFinaliza.split("-");
    console.log(fechaFinaliza);
    var secondDate = new Date(parseInt(fechaFinaliza[0]),parseInt(fechaFinaliza[1])-1,parseInt(fechaFinaliza[2]));

    $scope.proyecto.dias = Math.round((secondDate.getTime() - firstDate.getTime())/(oneDay));

    //Calculo los proyectos similares
    $scope.proyecto.similares = PROYECTOS.calculaSimilares($scope.proyecto);

    /**
     ** Metodo para volver al listado de proyectos
     ** @param filtrar (Boolean) Indica si se vuelve al conjunto de proyectos total o filtrado por ong
     */
    $scope.backProyectos = function(filtrar) {
        if (filtrar) $window.location.href = ('#/proyectos/0'); //Un cero como parametro para indicar que se carguen todos
        else $window.location.href = ('#/proyectos/'+$scope.proyecto.nombre);
    }

    /**
     ** Metodo para acceder a otro proyecto
     ** @param proyecto (Objeto) Objeto Proyecto al que se quiere acceder
     */
    $scope.goProyecto = function(proyecto) {
        window.location.href = ('#/proyecto/'+proyecto._id);
    }

    
    //Modal para donar
    $ionicModal.fromTemplateUrl('templates/donateModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.modal = modal
    });  
    
    $scope.openModal = function() {
        $scope.modal.show();
    };
  
    $scope.closeModal = function() {
        $scope.modal.hide();
    };


    //Modal para mostrar la descripcion
    $ionicModal.fromTemplateUrl('templates/descripcionModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.descripcionModal = modal
    });  
    
    $scope.openDescripcionModal = function() {
        $scope.descripcionModal.show();
    };
  
    $scope.closeDescripcionModal = function() {
        $scope.descripcionModal.hide();
    };

    $scope.$on('$destroy', function() {
        $scope.modal.remove();
        $scope.descripcionModal.remove();
    });


    //Modal para escoger como compartir
    $ionicModal.fromTemplateUrl('templates/shareModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.shareModal = modal
    });  
    
    $scope.openShareModal = function() {
        $scope.shareModal.show();
    };
  
    $scope.closeShareModal = function() {
        $scope.shareModal.hide();
    };   


    //Modal para mostrar el metodo de pago
    $scope.mascara = false;
    $ionicModal.fromTemplateUrl('templates/donateModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.donateModal = modal
        console.log(modal);
    });  
    
    $scope.openDonateModal = function() {
        $scope.mascara = true;
        $scope.donateModal.show();
    };
  
    $scope.closeDonateModal = function() {
        $scope.donateModal.hide();
    };  

    //Metodo para compartir el proyecto. Pasar como parametro la via
    $scope.shareBy = function(social) {
        
        SHARE.setDatos($scope.proyecto.nombre,'',$scope.proyecto.descripcion);

        var tipo = "",
            idProyecto = $scope.proyecto._id,
            fecha = new Date();
        
        if (social == 'facebook') {
            SHARE.facebookShare();
            tipo = "SHARE_FACEBOOK";
        }

        else if (social == 'twitter') {
            SHARE.twitterShare();
            tipo = "SHARE_TWITTER";
        }

        else if(social == 'whatsapp') {
            SHARE.whatsappShare();
            tipo = "SHARE_WHATSAPP";
        }

        else if(social == 'mail') {
            SHARE.mailShare();
            tipo = "SHARE_MAIL";
        }

        else if(social == 'pinterest') {
            SHARE.pinterestShare();
            tipo = "SHARE_PINTEREST";
        }

        else {
            SHARE.moreShare();
            tipo = "SHARE_OTHER";
        }

        //Agrego la actividad
        USER.addActividad(tipo,idProyecto,fecha);

        //Actualizo en el servidor
        USER.updateUserDataServer();

    };


    //Metodo para crear el mapa
    $scope.mapCreated = function(map) {
        $scope.map = map;
        $scope.map.setCenter(new google.maps.LatLng($scope.proyecto.latitud, $scope.proyecto.longitud));
    };



    //Objeto con la estrucutura para registrar la donacion
    $scope.donacion = {
        proyecto : 0.20,
        aenea : 0.01,
        total : 0.21
    };

    //Objeto con la estructura para registrar la tarjeta
    $scope.card = {
        numero : {
            parte1 : undefined,
            parte2 : undefined,
            parte3 : undefined,
            parte4 : undefined,
        },
        mes : undefined,
        anyo : undefined,
        cvc : undefined
    };

    //Inicio la trjeta si la tenia registrada
    if ($rootScope.getTarjetaLocalStorage() != null) {  //El usuario tiene tarjeta registrada
        $scope.card = $rootScope.getTarjetaLocalStorage();
        $scope.tieneTarjeta = true; 
        console.log("Tiene tarjeta");
    }
    else {
        $scope.tieneTarjeta = false;
        console.log("No tiene tarjeta");
    }


    //Pop up para configurar la donacion y confirmarla
    $scope.showPopup = function() {

        $ionicPopup.show({
            templateUrl: 'templates/donateModal.html',
            title: 'Tu donación',
            scope: $scope,
            buttons: [
                { text: 'Cancelar' },
                {
                    text: '<b>Confirmar</b>',
                    type: 'button-balanced',
                    onTap: function(e) {
                        //Ha pulsado confirmar
                        //Comprobamos que los numeros esten correctos
                        var parte1 = $scope.card.numero.parte1;
                        var parte2 = $scope.card.numero.parte2;
                        var parte3 = $scope.card.numero.parte3;
                        var parte4 = $scope.card.numero.parte4;
                        if (parte1 == undefined || parte2 == undefined || parte3 == undefined || parte4 == undefined) {
                            $rootScope.notify("Numero de tarjeta no válido",1000);
                            return;
                        }

                        if (parte1.toString().length != 4 || parte2.toString().length != 4 || parte3.toString().length != 4 || parte4.toString().length != 4) {
                            $rootScope.notify("Numero de tarjeta no válido",1000);
                            return;
                        }

                        if(parseInt(parte1) == NaN || parseInt(parte2) == NaN || parseInt(parte3) == NaN || parseInt(parte4) == NaN) {
                            $rootScope.notify("Numero de tarjeta no válido",1000);
                            return;
                        }

                        //Comprobamos el mes y el año
                        var mes = $scope.card.mes;
                        var anyo = $scope.card.anyo;
                        if (mes == undefined || anyo == undefined) {
                            $rootScope.notify("Fecha incorrecta",1000);
                            return;
                        }

                        if(parseInt(mes) > 12 || parseInt(mes) == NaN || anyo.toString().length != 2 || parseInt(anyo) == NaN) {
                            $rootScope.notify("Fecha incorrecta",1000);
                            return;
                        }

                        //Comrobamos el cvc
                        var cvc = $scope.card.cvc;
                        if (cvc == undefined) {
                            $rootScope.notify("Código incorrecto",1000);
                            return;
                        }

                        if(cvc.toString().length != 3 || parseInt(cvc) == NaN) {
                            $rootScope.notify("Código incorrecto",1000);
                            return;
                        }

                        //Toto esta ok, anotamos la tarjeta en el local storage sin el cvc
                        $scope.card.cvc = "";
                        $rootScope.setTarjetaLocalStorage($scope.card);

                        $rootScope.notify("Gracias por colaborar!",1500);

                        //Actualizamos datos del usuario (Tarjeta : SI y añadimos actividad)
                        USER.setTieneTarjeta(true);
                        USER.addActividadObjeto({
                            tipo : "DONATE",
                            cantidadAenea : $scope.donacion.aenea,
                            cantidadProyecto : $scope.donacion.proyecto, 
                            cantidadTotal : $scope.donacion.total,
                            proyectoAsociado : $scope.proyecto._id,
                            fecha : new Date()
                        })

                        //Actualizmaos usuario en el servidor
                        USER.updateUserDataServer();

                        //Actualizamos datos proyecto
                        $scope.proyecto.capitalRecaudado += $scope.donacion.proyecto;
                        $scope.proyecto.numeroDonantes++;

                        //Actualizamos datos del proyecto en el servidor
                        API.updateProject($rootScope.getToken(), $scope.proyecto._id, $scope.proyecto.capitalRecaudado, $scope.proyecto.numeroDonantes++ );

                    }
                }
            ]
        }).then(function(res) {
            
        });

    };


    $scope.clickGo21 = function() {
        $scope.donacion.aenea += 0.01;
        $scope.donacion.proyecto += 0.20;
        $scope.donacion.total = ($scope.donacion.aenea + $scope.donacion.proyecto).toFixed(2);
    }

    $scope.clickMore20 = function() {
        $scope.donacion.proyecto += 0.20;
        $scope.donacion.total = ($scope.donacion.aenea + $scope.donacion.proyecto).toFixed(2);
    }


    //Metodo para registrar un pago
    $scope.pagoCon = function(metodo) {
        if (metodo == 'tarjeta') {
            console.log($ionicSlideBoxDelegate);
            $ionicSlideBoxDelegate._instances[3].next(200);  // TODO Corregir esto!!
        }
        else {
            //Registro en el servidor el intento de configurar este pago
            USER.addActividadObjeto({
                tipo : "CONFIGURA_METODO_PAGO",
                metodo : metodo,
                proyectoAsociado : $scope.proyecto._id,
                fecha : new Date()
            })

            //Actualizmaos usuario en el servidor
            USER.updateUserDataServer();

            //Informo al usuario de que el método aún no está disponible
            $rootScope.notify("Metodo de pago aun no disponible",1500);
        }
    }


});
controladores.controller('RegisterCtrl', function ($rootScope, $scope, API, USER, $window) {
    
    //Datos a enviar para el registro, algunos los modificara el formulario, otros son los datos por defecto
    $scope.user = {
        email: "",
        password: "",
        nombre: "",
        social : "NO",
        preferencias : {},
        imagen : "",
        actividad : [],
        tarjeta : "NO"
    };

    //Metodo para llevar a cabo el registro
    $scope.createUser = function () {
        var email = this.user.email;
        var password = this.user.password;
        var uName = this.user.nombre;
        
        //Compruebo que se hayan introducido datos
        if(!email || !password || !uName) {
            $rootScope.notify("Datos no válidos",999);
            return false;
        }
        $rootScope.show('Registrando usuario...');
        
        //Registro una primera actividad de tipo CREATE si no esta ya registrada
        if (this.user.actividad.length == 0) {
            var actividad = {
                tipo : "CREATE",
                proyecto : -1,   //No esta asociada a ningun proyecto
                fecha : new Date()  //La fecha actual
            };
            this.user.actividad.push(actividad);
        }

        //Llamo a la API para procesar el registro
        API.signup(this.user).success(function (data) {
            console.log(data);  //Depurar si llegan bien los datos
            USER.setUser(data);  //Iniciamos USER
            $rootScope.setToken(email);  //Almaceno el token para crear sesion
            $rootScope.hide();
            $window.location.href = ('#/proyectos/0');
        }).error(function (error) {
            $rootScope.hide();
            if(error && error.error && error.error.code == 11000)
            {
                $rootScope.notify("Error. Ya existe un usuario con este correo",1500);
            }
            else
            {
                $rootScope.notify("Vaya...Se ha producido algun error. Inténtalo de nuevo",1500);
            }
            
        });
    }

    $scope.backInit = function() {
        $window.location.href = ('#/inicio');
    }
});controladores.controller('PerfilCtrl', function ($scope, $window, $rootScope, $cordovaCamera, $ionicModal, $ionicLoading, USER) {
    
    $scope.imagen =  "img/no-photo.png";
    if ($rootScope.getProfileImage()) $scope.imagen = $rootScope.getProfileImage();
    else if (USER.getImagen() != "") $scope.imagen = USER.getImagen();

    //Metodo para tomar la foto desde la camara
    $scope.sacarFoto = function() {
        var options = {
            quality: 50,
            allowEdit : true,
            destinationType: Camera.DestinationType.FILE_URL,
            sourceType: Camera.PictureSourceType.CAMERA
        };
        $cordovaCamera.getPicture(options).then( function(imageData) {
            $scope.imagen = imageData;
            //Almaceno la imagen en la memoria del dispositivo
            $rootScope.setProfileImage(imageData);
        },
        function(err){
            
        })
    }


    //Metodo para elegir una foto de la galeria
    $scope.elegirFoto = function() { 
        var options = {
            quality: 50,
            allowEdit : true,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
            window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
                $scope.imagen = fileEntry.nativeURL;
                //Almaceno la imagen en la memoria del dispositivo
                $rootScope.setProfileImage(fileEntry.nativeURL);
            });
        }, function(err){
            
        });
    }

    //Obtengo los datos del usuario
    $scope.datos = {
        nombre : USER.getNombre(),
        tarjeta : USER.tieneTarjeta()
    }
    $scope.preferencias = USER.getPreferencias();

    //Si tiene almacenada tarjeta recuperamos su valor
    $scope.card = {
        numero : 50,
        mes : "10",
        anyo : "12"
    };
    if($scope.datos.tarjeta) {
        if ($rootScope.getTarjetaLocalStorage() != undefined) {
            $scope.card = $rootScope.getTarjetaLocalStorage();
            console.log($rootScope.getTarjetaLocalStorage());
        }
    }

    //Monedas
    $scope.monedas = [
        "EURO",
        "DOLAR",
        "LIBRA"
    ];


    //Metodo para cambiar de moneda
    $scope.cambiarMoneda = function(moneda) {
        $scope.preferencias.moneda = moneda;
        $scope.closeEscogeMonedaModal();
    }

    //Metodo para cerrar sesion
    $scope.logout = function() {
        $rootScope.clearLocalStorage();  //Limpio cache para que se recarguen los datos
        //Apunto tambien en el servidor que no tiene tarjeta
        USER.setTieneTarjeta(false);
        USER.updateUserDataServer();
        $window.location.href = ('#/inicio');
        $rootScope.doRefresh();
    }


    $scope.actualizaTarjeta = function() {
        console.log("Hola: " + $scope.card.numero.length);
        if($scope.card.numero.length == 4 || $scope.card.numero.length == 9 || $scope.card.numero.length == 14) {
            $scope.card.numero = $scope.card.numero + "-";
        }
    }

    $scope.guardaTarjeta = function() {
        console.log($scope.card);
        if ($scope.card.numero.length != 19 || $scope.card.mes.length != 2 || $scope.card.anyo.length != 2) {
            $rootScope.notify("Datos incorrectos",999);
            return;
        }
        //Si llega aqui los datos esta ok, los almaceno
        $scope.datos.tarjeta = true;  //Anotamos en los datos del usuario que ha registrado una tarjeta
        $rootScope.setTarjetaLocalStorage($scope.card);  //Anotamos los datos de la tarjeta en la memoria del dispotivo
        $scope.closeConfiguraTarjetaModal();
    }


    //Metodo para guardar cambios
    $scope.backProyectos = function(guarda) {
        if (guarda) {
            var usuario = {
                _id: USER.getId(),
                email: USER.getEmail(),
                nombre: $scope.datos.nombre,
                social: USER.getSocial(),
                preferencias : $scope.preferencias,
                imagen : $scope.imagen,
                actividad : USER.getActividad(),
                tarjeta : ($scope.datos.tarjeta ? "SI" : "NO")
            };
            //Añado actividad de perfil mdificado
            usuario.actividad.push({
                tipo: 'PERFILMODIFICADO',
                proyecto: '-1',
                fecha: new Date()
            });
            console.log(usuario);
            USER.setUser(usuario);
            USER.updateUserDataServer();
            $window.location.href = ('#/proyectos/0');
        }
        else $window.location.href = ('#/proyectos/0');
        $rootScope.doRefresh();
    }



    //-----------------------------------------
    //                  MODALS
    //-----------------------------------------
    
    //Escoger moneda
    $ionicModal.fromTemplateUrl('templates/escogeMonedaModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.escogeMonedaModal = modal
    });  
    
    $scope.openEscogeMonedaModal = function() {
        $scope.escogeMonedaModal.show();
    };
  
    $scope.closeEscogeMonedaModal = function() {
        $scope.escogeMonedaModal.hide();
    };   

    //Esocoger metodo cambiar foto
    $ionicModal.fromTemplateUrl('templates/subirFotoModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.subirFotoModal = modal
    });  
    
    $scope.openSubirFotoModal = function() {
        $scope.subirFotoModal.show();
    };
  
    $scope.closeSubirFotoModal = function() {
        $scope.subirFotoModal.hide();
    };  


    //Configurar tarjeta modal
    $ionicModal.fromTemplateUrl('templates/configuraTarjetaModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.configuraTarjetaModal = modal
    });  
    
    $scope.openConfiguraTarjetaModal = function() {
        $scope.configuraTarjetaModal.show();
    };
  
    $scope.closeConfiguraTarjetaModal = function() {
        $scope.configuraTarjetaModal.hide();
    };  


});