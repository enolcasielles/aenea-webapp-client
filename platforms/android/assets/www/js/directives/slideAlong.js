

/**
 **Directiva que define la animacion para el menu
 */
directivas.directive('slideAlong', function($timeout, $ionicSideMenuDelegate) {
  return  {
    link: function($scope, $element, $attrs) {
      $scope.$watch(function() {
        return $ionicSideMenuDelegate.getOpenRatio();
      }, function(ratio) {

        // retrieve the offset value from the offset attribute
        var offset = parseInt($attrs.offset);

        // set the new position
        var position = $attrs.side == 'left' ? (ratio * (offset * -1)) + (offset) : (ratio * (offset * -1) - offset);

        // we want to set the transition to 500ms (arbitrary) when 
        // clicking/tapping and 0ms when swiping
        $element[0].style.webkitTransition = (ratio === 0 || ratio === 1 || ratio === -1) ? '200ms' : '0ms';

        // we set the offset according to the current ratio
        $element[0].style.webkitTransform = 'translate3d(' + position + '%, 0, 0)';

      });
    }
  };
});


