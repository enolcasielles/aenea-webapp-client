
/**
 ** Modulo para crear los servicios de la aplicacion
 */
var servicios = angular.module('aenea.services', []);

  servicios.factory('API', function ($rootScope, $http, $ionicLoading, $window) {
        
        var base = "https://secure-wildwood-4891.herokuapp.com";  //PRODUCCION
        //var base = "http://localhost:8100";                     //DESARROLLO

        $rootScope.show = function (text) {
            
            $rootScope.loading = $ionicLoading.show({
                content : 'Loading',
                template: text ? text : 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            
        };


        $rootScope.hide = function () {
            $ionicLoading.hide();
        };

        $rootScope.logout = function () {
            $rootScope.setToken("");
            $window.location.href = '#/inicio';
        };

        $rootScope.notify =function(text,duration){
            $rootScope.show(text);
            $window.setTimeout(function () {
              $rootScope.hide();
            }, duration);
        };

        
        $rootScope.doRefresh = function () {
            $rootScope.$broadcast('fetchAll');
            $rootScope.$broadcast('scroll.refreshComplete');
        };


        $rootScope.setToken = function (token) {
            return $window.localStorage.token = token;
        }

        $rootScope.getToken = function () {
            return $window.localStorage.token;
        };

        $rootScope.setFacebookToken = function(token) {
            $window.localStorage.fbtoken = token;
        };

        $rootScope.getFacebookToken = function() {
            return $window.localStorage.fbtoken;
        }

        $rootScope.setProfileImage = function(image) {
            $window.localStorage.imagenPerfil = image;
        }

        $rootScope.getProfileImage = function() {
            return $window.localStorage.imagenPerfil;
        }

        /**
         ** Almacena en la memoria local del dispositivo un objeto que almacena los datos de la tarjeta
         ** @param tarjeta (Objeto) objeto con los datos de la tarjeta
         */
        $rootScope.setTarjetaLocalStorage = function(tarjeta) {
            $window.localStorage.tarjeta = JSON.stringify(tarjeta);
        }

        $rootScope.getTarjetaLocalStorage = function() {
            if ($window.localStorage.tarjeta) return JSON.parse($window.localStorage.tarjeta);
            return null;
        }

        $rootScope.isSessionActive = function () {
            console.log(typeof $window.localStorage.token);
            return $window.localStorage.token ? true : false;
        }

        $rootScope.clearLocalStorage = function() {
            $window.localStorage.clear();
        }

        return {
            signin: function (form) {
                return $http.post(base+'/api/v1/aenea/usuarios/login', form);
            },
            signup: function (form) {
                return $http.post(base+'/api/v1/aenea/usuarios/registra', form);
            },
            getUser : function(email) {
                return $http.get(base+'/api/v1/aenea/usuarios/usuario/' + email, {
                    method : 'GET',
                    headers: {
                        token: email
                    }
                });
            },
            updateProject : function(email, id, capital, donantes) {
                var datos = {
                    capitalRecaudado : capital,
                    numeroDonantes : donantes
                }
                return $http.put(base+'/api/v1/aenea/proyectos/proyecto/' + id, datos, {
                    method : 'PUT',
                    headers : {
                        token : email
                    }
                });
            },
            updateUser : function(user) {
                return $http.put(base+'/api/v1/aenea/usuarios/usuario/' + user.id, user, {
                    method: 'PUT',
                    headers: {
                        token: user.email
                    }
                });
            },
            getAll: function (email) {
                return $http.get(base+'/api/v1/aenea/proyectos/all', {
                    method: 'GET',
                    headers: {
                        token: email
                    }
                });
            },
            getOne: function (id, email) {
                return $http.get(base+'/api/v1/aenea/proyectos/proyecto/' + id, {
                    method: 'GET',
                    headers: {
                        token: email
                    }
                });
            },
            getImage : function(url,email) {
                return $http.get(base+'/api/v1/aenea/proyectos/imagen/'+url , {
                    method : 'GET',
                    headers : {
                        token : email
                    }
                });
            },
            getBase : function() {
                return base;
            }
        }
    });    servicios.factory('USER',function(API) {

        var user = {};

        return {

            /**
             **Establece los datos del usuario
             */
            setUser : function(usuario) {
                user.id = usuario._id;
                user.email = usuario.email;
                user.nombre = usuario.nombre;
                user.imagen = usuario.imagen;
                user.social = usuario.social;
                user.preferencias = usuario.preferencias;
                user.tarjeta = usuario.tarjeta;
                user.actividad = usuario.actividad;
            },


            /**
             ** Actualiza los datos que haya en este momento del usario en el servidor
             ** Devuelve true si se ha actualizado correctamente o false si ocurre algun problema
             */
            updateUserDataServer : function() {
                API.updateUser(user).success(function (data) {
                    return true;
                }).error(function (error) {
                    return false;
                });
            },

            /**
             ** Devuelve el nombre del usuario
             */
            getNombre : function() {
                return user.nombre;
            },

            /**
             ** Devuelve el id del usuario
             */
            getId : function() {
                return user.id;
            },

            /**
             ** Devuelve el email del usuario
             */
            getEmail : function() {
                return user.email;
            },

            /**
             ** Devuelve la url con iamgen
             */
            getImagen : function() {
                return user.imagen;
            },

            /**
             ** Devuelve las preferencias del usuario
             */
            getPreferencias : function() {
                return user.preferencias;
            },

            /**
             ** Devuelve la red por la que el usuario se registra
             */
            getSocial : function() {
                return user.social;
            },


            /**
             ** True si el usuario ha registrado tarjeta o false si no
             */
            tieneTarjeta : function() {
                if(user.tarjeta == 'SI') return true;
                return false;
            },

            /**
             ** Establece si el usuario ha registrado tarjeta o no
             ** @param bol (Boolean) true si ha registrado tarjeta o false si no
             */
            setTieneTarjeta : function(bol) {
                if (bol) user.tarjeta = "SI";
                else user.tarjeta = "NO";
            },

            /**
             ** Devuelve la actividad del usuario
             */
            getActividad : function() {
                return user.actividad;
            },


            /**
             ** Registra una nueva actividad 
             ** @param tipo (String) El tipo de la actividad (CREATE, SHAREFACEBOOK, DONATE,...)
             ** @param idProyecto (Number) Un entero que represente al proyecto asociado a esta actividad
             ** @param fecha (Date) Representa la fecha en la que ocurre
             */
            addActividad : function(tipo,idProyecto,fecha) {
                user.actividad.push({
                    tipo : tipo,
                    proyectoAsociado : idProyecto,
                    fecha : fecha
                });
            },


            /**
             ** Registra una actividad a partir del objeto que recibe
             ** @param obj (Objeto) el objeto que almacena los datos de la actividad
             */
            addActividadObjeto : function(obj) {
                user.actividad.push(obj);
            }


        }

    });
    servicios.factory('PROYECTOS', function (API) {


            var ramas = [
                {
                    nombre : 'DEVELOPMENT', 
                    imagenSeleccionada : 'img/iconos_ramas/development_pulsado.png',
                    imagenNormal : 'img/iconos_ramas/development.png'
                },
                {
                    nombre : 'CHILDHOODCARE',
                    imagenSeleccionada : 'img/iconos_ramas/child_hood_care_pulsado.png',
                    imagenNormal : 'img/iconos_ramas/child_hood_care.png'
                },
                {
                    nombre : 'ECOLOGY',
                    imagenSeleccionada : 'img/iconos_ramas/ecology_pulsado.png',
                    imagenNormal : 'img/iconos_ramas/ecology.png'
                },
                {
                    nombre : 'EMERGENCIES',
                    imagenSeleccionada : 'img/iconos_ramas/emergencies_pulsado.png',
                    imagenNormal : 'img/iconos_ramas/emergencies.png'
                },
                {
                    nombre : 'HEALTHSAFETY',
                    imagenSeleccionada : 'img/iconos_ramas/health_safety_pulsado.png',
                    imagenNormal : 'img/iconos_ramas/health_safety.png'
                },
                {
                    nombre : 'SCIENTIFICRESEARCH',
                    imagenSeleccionada : 'img/iconos_ramas/scientific_research_pulsado.png',
                    imagenNormal : 'img/iconos_ramas/scientific_research.png'
                }
            ];

            var proyectos = [];
            var proyOrdenados = [];  //Cuando se aplica un criterio de ordenacion sobre proyectos
            var proyFiltrados = [];  //Cuando se aplica un filtro sobre proyOrdenados

            var idUltimoProyecto = 0;

            return {
                getRamas : function () {
                    return ramas;
                },

                get : function(id) {
                    for (var i=0 ; i<proyectos.length ; i++) {
                        if (proyectos[i]._id == id) return proyectos[i];
                    }
                },

                setProyectos : function(proy) {
                    proyectos = proy;
                    proyOrdenados = proy;
                    idUltimoProyecto = proy[proy.length-1]._id;  //Anoto el id del ultimo proyecto descargado para futuras actualizaciones
                },

                hayProyectos : function() {
                    return (proyectos.length !== 0);
                },

                filtra : function(rama) {
                    //Limpio el contenedor
                    proyFiltrados = [];
                    for (var i=0 ; i<proyOrdenados.length ; i++) { //Recorro todos los proyectos
                        var ramas = proyOrdenados[i].ramas;  //Recupero las ramas
                        for (var j=0 ; j<ramas.length ; j++) {  //Recorro las ramas
                            if (ramas[j] == rama) {  //Si pertenece a esta rama lo añado y paso al siguiente proyecto
                                proyFiltrados.push(proyOrdenados[i]);
                                break;
                            }
                        }
                    }
                    return proyFiltrados;
                },

                filtraOng : function(ong) {
                    var proy = [];
                    for (var i=0 ; i<proyectos.length ; i++) {
                        if (proyectos[i].ong == ong) {
                            proy.push(proyectos[i]);
                        }
                    }
                    return proy;
                },

                ordenaReciente : function() {

                    //Limpio el contenedor
                    proyOrdenados = [];

                    //Recorro todos los proyectos calculando los dias transcurridos desde su creacion
                    for (var i=0 ; i<proyectos.length ; i++) {
                        proyOrdenados[i] = proyectos[i];
                        //Calculo los dias que han pasado
                        var oneDay = 24*60*60*1000; 
                        var actual = new Date();  //Fecha actual
                        var fechaCreacion = proyectos[i].fechaCreacion.split("-");
                        var creacion = new Date(parseInt(fechaCreacion[0]),parseInt(fechaCreacion[1])-1,parseInt(fechaCreacion[2]));
                        proyOrdenados[i].diasTranscurridos = Math.round((actual.getTime() - creacion.getTime())/(oneDay));
                    }

                    //Ahora ordeno por este valor
                    for (i=0 ; i<proyOrdenados.length-1 ; i++) {
                        //Calculo el minimo de i
                        var min = proyOrdenados[i].diasTranscurridos;
                        var pos = i;  //La posicion en la que se encientra
                        for (var j=i+1 ; j<proyOrdenados.length ; j++) {
                            if(proyOrdenados[j].diasTranscurridos < min) {
                                min = proyOrdenados[j].diasTranscurridos;
                                pos = j;
                            } 
                        }
                        //Intercambio las posiciones i por pos para colocar el minimo en i
                        var proyTemp = proyOrdenados[i];
                        proyOrdenados[i] = proyOrdenados[pos];
                        proyOrdenados[pos] = proyTemp;
                    }

                    return proyOrdenados;
                },


                ordenaFechaFinaliza : function() {

                    proyOrdenados = [];

                    //Recorro todos los proyectos calculando los dias transcurridos desde su creacion
                    for (var i=0 ; i<proyectos.length ; i++) {
                        proyOrdenados[i] = proyectos[i];
                        //Calculo los dias que han pasado
                        var oneDay = 24*60*60*1000; 
                        var actual = new Date();  //Fecha actual
                        var fechaFinaliza = proyectos[i].fechaFinaliza.split("-");
                        var finaliza = new Date(parseInt(fechaFinaliza[0]),parseInt(fechaFinaliza[1])-1,parseInt(fechaFinaliza[2]));
                        proyOrdenados[i].diasFinaliza = Math.round((finaliza.getTime() - actual.getTime())/(oneDay));
                    }
                   

                    //Ordeno por el dato de los dias que falta para que finalice
                    for (i=0 ; i<proyOrdenados.length-1 ; i++) {
                        //Calculo el minimo de i
                        var min = proyOrdenados[i].diasFinaliza;
                        var pos = i;  //La posicion en la que se encientra
                        for (var j=i+1 ; j<proyOrdenados.length ; j++) {
                            if(proyOrdenados[j].diasFinaliza < min) {
                                min = proyOrdenados[j].diasFinaliza;
                                pos = j;
                            } 
                        }
                        //Intercambio las posiciones i por pos para colocar el minimo en i
                        var proyTemp = proyOrdenados[i];
                        proyOrdenados[i] = proyOrdenados[pos];
                        proyOrdenados[pos] = proyTemp;
                    }

                    return proyOrdenados;

                },


                ordenaCompletado : function() {
                    
                    proyOrdenados = [];

                    //Recorro todos los proyectos calculando el porcentaje completado
                    for (var i=0 ; i<proyectos.length ; i++) {
                        proyOrdenados[i] = proyectos[i];
                        proyOrdenados[i].completado = parseFloat(proyOrdenados[i].capitalRecaudado) / parseFloat(proyOrdenados[i].presupuesto);
                    }
                   

                    //Ordeno por el dato de el porcentaje completao que falta para que finalice
                    for (i=0 ; i<proyOrdenados.length-1 ; i++) {
                        //Calculo el maximo desde i
                        var max = proyOrdenados[i].completado;
                        var pos = i;  //La posicion en la que se encientra el maximo
                        for (var j=i+1 ; j<proyOrdenados.length ; j++) {
                            if(proyOrdenados[j].completado > max) {
                                max = proyOrdenados[j].completado;
                                pos = j;
                            } 
                        }
                        //Intercambio las posiciones i por pos para colocar el minimo en i
                        var proyTemp = proyOrdenados[i];
                        proyOrdenados[i] = proyOrdenados[pos];
                        proyOrdenados[pos] = proyTemp;
                    }

                    return proyOrdenados;

                },  


                ordenaNumDonantes : function() {

                    proyOrdenados = proyectos;


                    //Ordeno por el numero de donantes
                    for (i=0 ; i<proyOrdenados.length-1 ; i++) {

                        //Calculo el maximo desde i
                        var max = parseInt(proyOrdenados[i].numeroDonantes);
                        var pos = i;  //La posicion en la que se encientra
                        for (var j=i+1 ; j<proyOrdenados.length ; j++) {
                            if(parseInt(proyOrdenados[j].numeroDonantes) > max) {
                                max = parseInt(proyOrdenados[j].numeroDonantes);
                                pos = j;
                                console.log("OK");
                            } 
                        }
                        //Intercambio las posiciones i por pos para colocar el maximo en i
                        var proyTemp = proyOrdenados[i];
                        proyOrdenados[i] = proyOrdenados[pos];
                        proyOrdenados[pos] = proyTemp;
                    }

                    return proyOrdenados;

                },

                muestraTodos : function() {
                    proyOrdenados = proyectos;
                    return proyOrdenados;
                },

                quitaFiltros : function() {
                    return proyOrdenados;
                },

                buscar : function(key) {
                    var proy = [];
                    key = key.toLowerCase();  //Cambio a minusculas
                    for (var i=0 ; i<proyectos.length ; i++) {
                        //Busco en el nombre y en la ong
                        if(proyectos[i].nombre.toLowerCase().indexOf(key) > -1 
                            || proyectos[i].ong.toLowerCase().indexOf(key) > -1) {
                                proy.push(proyectos[i]);
                        }
                    }
                    console.log("buscar: " + key);
                    return proy;
                },

                
                /**
                 ** Devuelve los 3 proyectos mas similares a uno pasado como parametro
                 ** @param proyecto (Objeto) objeto que almacena los datos del proyecto con el que buscar los mas similares
                 ** return Array con 3 objetos que representan los proyectos mas similares
                 */
                calculaSimilares : function(proyecto) {

                    //Caluclo dias totales y dias de vida que le quedan al proyecto
                    var oneDay = 24*60*60*1000; 
                    var actual = new Date();  //Fecha actual
                    var fechaFinaliza = proyecto.fechaFinaliza.split("-");
                    var finaliza = new Date(parseInt(fechaFinaliza[0]),parseInt(fechaFinaliza[1])-1,parseInt(fechaFinaliza[2]));
                    var diasFinalizaProyecto = Math.round((finaliza.getTime() - actual.getTime())/(oneDay));
                   
                    var fechaCreacion = proyecto.fechaCreacion.split("-");
                    var creacion = new Date(parseInt(fechaCreacion[0]),parseInt(fechaCreacion[1])-1,parseInt(fechaCreacion[2]));
                    var diasTotalesProyecto = Math.round((finaliza.getTime() - creacion.getTime())/(oneDay));

                    for (var i=0 ; i<proyectos.length ; i++) {
                        var proyectoActual = proyectos[i];
                        if(proyectoActual._id == proyecto._id) continue;
                        var puntos = 0;  //Variable que almacena la puntuacion de cada proyecto
                        
                        //Comparo las ramas
                        for (var j=0 ; j<proyecto.ramas.length ; j++) {
                            for (var jj=0 ; jj<proyectoActual.ramas.length ; jj++) {
                                if (proyecto.ramas[j] == proyectoActual.ramas[jj]) puntos+=10;
                            }
                        }

                        //Calculo dias totales y dias de vida del proyecto actual
                        fechaFinaliza = proyectoActual.fechaFinaliza.split("-");
                        finaliza = new Date(parseInt(fechaFinaliza[0]),parseInt(fechaFinaliza[1])-1,parseInt(fechaFinaliza[2]));
                        var diasFinalizaProyectoActual = Math.round((finaliza.getTime() - actual.getTime())/(oneDay));
                        fechaFinalizacion = proyectoActual.fechaCreacion.split("-");
                        creacion = new Date(parseInt(fechaCreacion[0]),parseInt(fechaCreacion[1])-1,parseInt(fechaCreacion[2]));
                        var diasTotalesProyectoActual = Math.round((finaliza.getTime() - creacion.getTime())/(oneDay));

                        //Comparo el presupuesto sobre el numero de dias totales
                        var presupuesto = proyecto.presupuesto / diasTotalesProyecto;
                        var presupuestoActual = proyectoActual.presupuesto / diasTotalesProyectoActual;
                        var diferencia=1.0;
                        if (presupuesto > presupuestoActual) {
                            diferencia = (presupuesto - presupuestoActual) / presupuesto;
                        }
                        else {
                            diferencia = (presupuestoActual - presupuesto) / presupuestoActual;
                        }
                        if (diferencia <= 0.2) puntos+=10;
                        if (diferencia > 0.2 && diferencia <= 0.4) puntos+=7; 
                        if (diferencia > 0.4 && diferencia <= 0.6) puntos+=3; 
                        if (diferencia > 0.6 && diferencia <= 0.8) puntos+=1; 

                        //Comparo el recaudado sobre el numero de dias que tiene
                        var recaudado = proyecto.capitalRecaudado / (diasTotalesProyecto - diasFinalizaProyecto);
                        var recaudadoActual = proyectoActual.capitalRecaudado / (diasTotalesProyecto - diasFinalizaProyecto);
                        diferencia=1.0;
                        if (recaudado > recaudadoActual) {
                            diferencia = (recaudado - recaudadoActual) / recaudado;
                        }
                        else {
                            diferencia = (recaudadoActual - recaudado) / recaudadoActual;
                        }
                        if (diferencia <= 0.2) puntos+=10;
                        if (diferencia > 0.2 && diferencia <= 0.4) puntos+=7; 
                        if (diferencia > 0.4 && diferencia <= 0.6) puntos+=3; 
                        if (diferencia > 0.6 && diferencia <= 0.8) puntos+=1; 

                        var numDonantes = proyecto.numeroDonantes / (diasTotalesProyecto - diasFinalizaProyecto);
                        var numDonantesActual = proyectoActual.numeroDonantes / (diasTotalesProyecto - diasFinalizaProyecto);
                        diferencia=1.0;
                        if (numDonantes > numDonantesActual) {
                            diferencia = (numDonantes - numDonantesActual) / numDonantes;
                        }
                        else {
                            diferencia = (numDonantesActual - numDonantes) / numDonantesActual;
                        }
                        if (diferencia <= 0.2) puntos+=10;
                        if (diferencia > 0.2 && diferencia <= 0.4) puntos+=7; 
                        if (diferencia > 0.4 && diferencia <= 0.6) puntos+=3; 
                        if (diferencia > 0.6 && diferencia <= 0.8) puntos+=1; 

                        proyectoActual.puntos = puntos;

                    }

                    //Finalmente cojo aquellos 3 proyectos con mayor puntuacion
                    var resultado = [];
                    for (var it=0 ; it<3 ; it++) {
                      var posicion = 0;
                      puntos = 0;
                      for (var i=0 ; i<proyectos.length ; i++) {
                        var repetido = false;
                        for (var j=0 ; j<resultado.length ; j++) {
                            if (proyectos[i]._id == resultado[j]._id) {
                                repetido = true;
                                break;
                            }
                        }
                        if (repetido) continue;
                        if (proyectos[i]._id == proyecto._id) continue;
                        if (proyectos[i].puntos > puntos) {
                            puntos = proyectos[i].puntos;
                            posicion = i;
                        }
                      }
                      resultado.push(proyectos[posicion]);
                    }

                    return resultado;
                }        


            }

    });
    servicios.factory('SHARE', function($cordovaSocialSharing) {

        var data = {};
        data.message = '';
        data.link = 'http://www.aenea.co';
        data.subject = '';

        return {
            
            moreShare : function() {
                return $cordovaSocialSharing.share(data.message, data.subject, null, data.link);
            },

            facebookShare : function() {
                return $cordovaSocialSharing.shareViaFacebook(data.message, null, data.link);
            },

            twitterShare : function() {
                return $cordovaSocialSharing.shareViaTwitter(data.message, null, data.link);
            },

            whatsappShare : function() {
                return $cordovaSocialSharing.shareViaWhatsApp(data.message, null, data.link);
            },

            mailShare : function() {
                return $cordovaSocialSharing.shareViaEmail(data.message, data.subject, null, null, null, null);
            },

            pinterestShare : function() {
                return $cordovaSocialSharing.shareViaPinterest(data.message, null);
            },

            setDatos : function(subject,link,message) {
                data.message = message;
                if (link != '') data.link = link;
                data.subject = subject;
            }
        }

    });
    //Factory que almacena los distintos Strings de la App en los distintos idiomas, ingles y español
    servicios.factory('STRINGS',function() {

        //Constante que define el numero de idiomas
        var NUM_IDIOMAS = 2;

        return {

            //Constantes que definen los distintos idiomas
            ES : 0, 
            EN : 1,

            //Variable para definir el idioma en el que se mostrara la app
            idioma : this.EN,  //Por defecto ingles

            //Metodo para configurar el idioma
            setIdioma : function(idioma) {
                if(idioma >= NUM_IDIOMAS || idioma < 0) idioma = this.EN;  //Si es un valor no valido cogemos ingles
                else this.idioma = idioma;
            },

            
            //Definir aqui todos los strings que se quiera utiliza en la App como un array en el 
            //que cada elemento se corresponde a un idioma en el mismo orden que se declaran los idiomas
            tituloLogin : ['Inicio','Login'],


        }

    });
