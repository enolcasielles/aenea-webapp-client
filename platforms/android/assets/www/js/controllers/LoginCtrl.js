/*
 *Controlador para manejar el estado Login, define un objeto user en el scope con los campos email y password
 *Ademas define un metodo en el scope para validar el usuario. 
 */
controladores.controller('LoginCtrl', function ($rootScope, $scope, API, USER, $window) {

    //Datos que se precisa para validar el login
    $scope.user = {
        email: "",
        password: ""
    };


    //Metodo para validar el usuario
    $scope.validateUser = function () {
        var email = this.user.email;
        var password = this.user.password;
        if(!email || !password) {
            $rootScope.notify("Error. Datos no válidos",999);
            return false;
        }
        $rootScope.show('Comprobando datos...');
        API.signin($scope.user).success(function (data) {
            USER.setUser(data);  //Iniciamos USER
            $rootScope.setToken(email); 
            $rootScope.hide();
            $window.location.href = ('#/proyectos/0');
        }).error(function (error) {
            console.log(error);
            $rootScope.hide();
            if (error && error.code == 1) {  //Email no registrado
                $rootScope.notify("Error. Correo no registrado",1500);
            }
            else if (error && error.code == 2) {  //Contraseña incorrecta
                $rootScope.notify("Error. Contraseña incorrecta",1500);
            }
            else {
                $rootScope.notify("Vaya...Se ha producido algun error. Inténtalo de nuevo",1500);
            }
        });
    }

    //Metodo para volver a inicio
    $scope.backInit = function() {
        $window.location.href = ('#/inicio');
    }

});