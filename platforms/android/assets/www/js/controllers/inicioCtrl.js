/*
 *Controlador para manejar el estado inicio, que implementa la funcionalidad para iniciar
 *sesion con facebook o twitter. 
 */
controladores.controller('InicioCtrl', function ($state, $rootScope, $scope, $window, API, USER, $cordovaOauth, $http){
    

    //Si el usuario ya tiene sesion iniciada accedemos a la app
    if ($rootScope.isSessionActive()) {
        //Recupero datos del usuario
        var email = $rootScope.getToken();
        API.getUser(email).success(function (data) {
           USER.setUser(data);
           $window.location.href = ('#/proyectos/0'); 
        }).error(function (error) {
            $rootScope.notify("Error en el servidor. Imposible obtener datos",1999);
        });
    }


    //Metodo para iniciar con facebook
    $scope.facebookLogin = function() {
        $cordovaOauth.facebook("276563885847575",["email"]).then(function(result) {
            //Recupero datos que preciso del usuario
            var access_token = result.access_token;
            $http.get("https://graph.facebook.com/v2.3/me", { params: { access_token: result.access_token, fields: "id,name,email,picture", format: "json" }}).then(function(result) {
                $rootScope.show("Iniciando...");
                var usuario = {
                    email: result.data.email,
                    password: "",
                    nombre: result.data.name,
                    social: "FACEBOOK",
                    preferencias : {},
                    imagen : result.data.picture.data.url,
                    actividad : [],
                    tarjeta : "NO"
                };
                usuario.actividad.push({
                    tipo : "CREATE",
                    proyecto : -1,   //No esta asociada a ningun proyecto
                    fecha : new Date()  //La fecha actual
                });
                API.signup(usuario).success(function(data) {
                    //El servidor me devolvera la misma estructura de datos si era el primer registro o la 
                    //que hubiese almacenada si no era un nuevo usuario. Sea como sea almaceno la que me envia
                    USER.setUser(data);
                    $rootScope.setToken(data.email);
                    $rootScope.setFacebookToken(access_token);
                    $rootScope.hide();
                    $window.location.href = ('#/proyectos/0');
                });
            }, function(error) {
                $rootScope.notify("Algun error se produjo, inténtalo de nuevo",999);
                console.log(error);
            });
            
        }, function(error) {
            $rootScope.notify("Algun error se produjo, inténtalo de nuevo",999);
            console.log(error);
        });
    }
    

});
