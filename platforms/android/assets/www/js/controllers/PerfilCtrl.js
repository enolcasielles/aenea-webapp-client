controladores.controller('PerfilCtrl', function ($scope, $window, $rootScope, $cordovaCamera, $ionicModal, $ionicLoading, USER) {
    
    $scope.imagen =  "img/no-photo.png";
    if ($rootScope.getProfileImage()) $scope.imagen = $rootScope.getProfileImage();
    else if (USER.getImagen() != "") $scope.imagen = USER.getImagen();

    //Metodo para tomar la foto desde la camara
    $scope.sacarFoto = function() {
        var options = {
            quality: 50,
            allowEdit : true,
            destinationType: Camera.DestinationType.FILE_URL,
            sourceType: Camera.PictureSourceType.CAMERA
        };
        $cordovaCamera.getPicture(options).then( function(imageData) {
            $scope.imagen = imageData;
            //Almaceno la imagen en la memoria del dispositivo
            $rootScope.setProfileImage(imageData);
        },
        function(err){
            
        })
    }


    //Metodo para elegir una foto de la galeria
    $scope.elegirFoto = function() { 
        var options = {
            quality: 50,
            allowEdit : true,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY
        };

        $cordovaCamera.getPicture(options).then(function(imageURI) {
            window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
                $scope.imagen = fileEntry.nativeURL;
                //Almaceno la imagen en la memoria del dispositivo
                $rootScope.setProfileImage(fileEntry.nativeURL);
            });
        }, function(err){
            
        });
    }

    //Obtengo los datos del usuario
    $scope.datos = {
        nombre : USER.getNombre(),
        tarjeta : USER.tieneTarjeta()
    }
    $scope.preferencias = USER.getPreferencias();

    //Si tiene almacenada tarjeta recuperamos su valor
    $scope.card = {
        numero : 50,
        mes : "10",
        anyo : "12"
    };
    if($scope.datos.tarjeta) {
        if ($rootScope.getTarjetaLocalStorage() != undefined) {
            $scope.card = $rootScope.getTarjetaLocalStorage();
            console.log($rootScope.getTarjetaLocalStorage());
        }
    }

    //Monedas
    $scope.monedas = [
        "EURO",
        "DOLAR",
        "LIBRA"
    ];


    //Metodo para cambiar de moneda
    $scope.cambiarMoneda = function(moneda) {
        $scope.preferencias.moneda = moneda;
        $scope.closeEscogeMonedaModal();
    }

    //Metodo para cerrar sesion
    $scope.logout = function() {
        $rootScope.clearLocalStorage();  //Limpio cache para que se recarguen los datos
        //Apunto tambien en el servidor que no tiene tarjeta
        USER.setTieneTarjeta(false);
        USER.updateUserDataServer();
        $window.location.href = ('#/inicio');
        $rootScope.doRefresh();
    }


    $scope.actualizaTarjeta = function() {
        console.log("Hola: " + $scope.card.numero.length);
        if($scope.card.numero.length == 4 || $scope.card.numero.length == 9 || $scope.card.numero.length == 14) {
            $scope.card.numero = $scope.card.numero + "-";
        }
    }

    $scope.guardaTarjeta = function() {
        console.log($scope.card);
        if ($scope.card.numero.length != 19 || $scope.card.mes.length != 2 || $scope.card.anyo.length != 2) {
            $rootScope.notify("Datos incorrectos",999);
            return;
        }
        //Si llega aqui los datos esta ok, los almaceno
        $scope.datos.tarjeta = true;  //Anotamos en los datos del usuario que ha registrado una tarjeta
        $rootScope.setTarjetaLocalStorage($scope.card);  //Anotamos los datos de la tarjeta en la memoria del dispotivo
        $scope.closeConfiguraTarjetaModal();
    }


    //Metodo para guardar cambios
    $scope.backProyectos = function(guarda) {
        if (guarda) {
            var usuario = {
                _id: USER.getId(),
                email: USER.getEmail(),
                nombre: $scope.datos.nombre,
                social: USER.getSocial(),
                preferencias : $scope.preferencias,
                imagen : $scope.imagen,
                actividad : USER.getActividad(),
                tarjeta : ($scope.datos.tarjeta ? "SI" : "NO")
            };
            //Añado actividad de perfil mdificado
            usuario.actividad.push({
                tipo: 'PERFILMODIFICADO',
                proyecto: '-1',
                fecha: new Date()
            });
            console.log(usuario);
            USER.setUser(usuario);
            USER.updateUserDataServer();
            $window.location.href = ('#/proyectos/0');
        }
        else $window.location.href = ('#/proyectos/0');
        $rootScope.doRefresh();
    }



    //-----------------------------------------
    //                  MODALS
    //-----------------------------------------
    
    //Escoger moneda
    $ionicModal.fromTemplateUrl('templates/escogeMonedaModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.escogeMonedaModal = modal
    });  
    
    $scope.openEscogeMonedaModal = function() {
        $scope.escogeMonedaModal.show();
    };
  
    $scope.closeEscogeMonedaModal = function() {
        $scope.escogeMonedaModal.hide();
    };   

    //Esocoger metodo cambiar foto
    $ionicModal.fromTemplateUrl('templates/subirFotoModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.subirFotoModal = modal
    });  
    
    $scope.openSubirFotoModal = function() {
        $scope.subirFotoModal.show();
    };
  
    $scope.closeSubirFotoModal = function() {
        $scope.subirFotoModal.hide();
    };  


    //Configurar tarjeta modal
    $ionicModal.fromTemplateUrl('templates/configuraTarjetaModal.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.configuraTarjetaModal = modal
    });  
    
    $scope.openConfiguraTarjetaModal = function() {
        $scope.configuraTarjetaModal.show();
    };
  
    $scope.closeConfiguraTarjetaModal = function() {
        $scope.configuraTarjetaModal.hide();
    };  


});