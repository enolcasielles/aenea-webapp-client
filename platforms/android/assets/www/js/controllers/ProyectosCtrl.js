controladores.controller('ProyectosCtrl', function ($rootScope, $scope, $ionicHistory, $stateParams, API, USER, $ionicScrollDelegate, $ionicModal, $window, PROYECTOS, $ionicSideMenuDelegate) {


    $rootScope.$on('fetchAll', function(){
        
        $scope.ramas = PROYECTOS.getRamas();

        $scope.ramaSeleccionada = '';


        $rootScope.show("Obteniendo...");
        API.getAll($rootScope.getToken()).success(function (data, status, headers, config) {
            $scope.proyectos = [];

            var ong = $stateParams.nombre;
            var filtrar = ong != '0' ? true : false;

            console.log("Ong: " + ong + " ; filtrar: " + filtrar);

            for (var i=0 ; i<data.length ; i++) {

                if (filtrar && ong != data.ong) {
                    continue;
                }

                $scope.proyectos.push(data[i]);

                //Url de la imagen principal
                $scope.proyectos[i].imagen = API.getBase() + $scope.proyectos[i].imagen;

                //Url de la imagen de la ong
                $scope.proyectos[i].imagenOng = API.getBase() + $scope.proyectos[i].imagenOng;

                //Url de las imagenes del slider
                $scope.proyectos[i].imagenesSlider = $scope.proyectos[i].imagenesSlider.split(";");
                for (var j=0 ; j<$scope.proyectos[i].imagenesSlider.length ; j++) {
                    $scope.proyectos[i].imagenesSlider[j] = API.getBase() +  $scope.proyectos[i].imagenesSlider[j];
                }
                
                //Almaceno las ramas a la que pertenece
                $scope.proyectos[i].ramas = $scope.proyectos[i].rama.split(";");

            };
            if($scope.proyectos.length == 0) {
                $scope.noData = true;
            }
            else
            {
                $scope.noData = false;
                PROYECTOS.setProyectos($scope.proyectos);
            }

            //Añado al scope la informaion del usuario para formar el menu
            $scope.usuario = {};
            $scope.usuario.imagen =  "img/no-photo.png";
            if ($rootScope.getProfileImage()) $scope.usuario.imagen = $rootScope.getProfileImage();
            else if (USER.getImagen() != "") $scope.usuario.imagen = USER.getImagen();
            $scope.usuario.nombre = USER.getNombre();

            $rootScope.hide();
        }).error(function (data, status, headers, config) {
            $rootScope.hide();
            $rootScope.notify("Ocurrio un problema al recuperar los datos. Intentelo de nuevo mas tarde");
        });

    });

    $rootScope.$broadcast('fetchAll');


    //Variable que controla la visibilidad del buscador
    $scope.showBuscador = false;

    //Metodo para ocultar y/o mostrar el buscador
    $scope.cambiaVisibilidadBuscador = function() {
        if (this.showBuscador) {  //Si estoy cerrando el buscador muestro todos
            $scope.proyectos = PROYECTOS.muestraTodos();
            this.keyBuscar = '';  //Reinicio la palabra a buscar
        }
        else {  //Si estoy abriendo el buscador
            //Limpio el filtro por si habia alguno activo
            $scope.ramaSeleccionada = '';
        }
        this.showBuscador = !this.showBuscador;  //Cambio el estado
    }

    //Metodo para buscar en los proyectos
    $scope.buscar = function() {
        if(this.keyBuscar.length < 3) return;
        $scope.proyectos = PROYECTOS.buscar(this.keyBuscar);
    }


    //Añado un metodo para sacar el menu
    $scope.sacaMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.filtra = function(rama) {

        if (rama == $scope.ramaSeleccionada) {  //Muestro todos
            $scope.proyectos = PROYECTOS.quitaFiltros();
            $scope.ramaSeleccionada = '';
            console.log('He pinchao en la seleccionada');
        }
        else {
            $scope.proyectos = PROYECTOS.filtra(rama);
            $scope.ramaSeleccionada = rama;
            
        }

        //Scroll top
        $ionicScrollDelegate.scrollTop();
    };

    $scope.goProyecto = function(id) {
        $window.location.href = ('#/proyecto/'+id);
    };

    $scope.goPerfil = function() {
        $window.location.href = ('#/perfil');
        $ionicSideMenuDelegate.toggleLeft(false);  //Oculto el menu
        $ionicHistory.clearCache();  //Limpio cache para que se recarguen los datos
        $ionicHistory.clearHistory();
    };

    //Metodo para ordenar el conjunto de proyectos. Se llamara cuando se pulse uno de los botones del menu
    $scope.ordena = function(filtro) {
        
        $ionicSideMenuDelegate.toggleLeft(false);  //Oculto el menu
        $ionicScrollDelegate.scrollTop();
        $scope.ramaSeleccionada = '';  //Quito el posible filtro
        
        if (filtro == 'recientes') {
            $scope.proyectos = PROYECTOS.ordenaReciente();
        }

        else if(filtro == 'finaliza') {
            $scope.proyectos = PROYECTOS.ordenaFechaFinaliza();
        }

        else if(filtro == 'completado') {
            $scope.proyectos = PROYECTOS.ordenaCompletado();
        }

        else if(filtro == 'donaciones') {
            $scope.proyectos = PROYECTOS.ordenaNumDonantes();
        }

        else {
            //Muestro todos
            $scope.proyectos = PROYECTOS.muestraTodos();
        }
    }


});
