  servicios.factory('API', function ($rootScope, $http, $ionicLoading, $window) {
        
        var base = "https://secure-wildwood-4891.herokuapp.com";  //PRODUCCION
        //var base = "http://localhost:8100";                     //DESARROLLO

        $rootScope.show = function (text) {
            
            $rootScope.loading = $ionicLoading.show({
                content : 'Loading',
                template: text ? text : 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            
        };


        $rootScope.hide = function () {
            $ionicLoading.hide();
        };

        $rootScope.logout = function () {
            $rootScope.setToken("");
            $window.location.href = '#/inicio';
        };

        $rootScope.notify =function(text,duration){
            $rootScope.show(text);
            $window.setTimeout(function () {
              $rootScope.hide();
            }, duration);
        };

        
        $rootScope.doRefresh = function () {
            $rootScope.$broadcast('fetchAll');
            $rootScope.$broadcast('scroll.refreshComplete');
        };


        $rootScope.setToken = function (token) {
            return $window.localStorage.token = token;
        }

        $rootScope.getToken = function () {
            return $window.localStorage.token;
        };

        $rootScope.setFacebookToken = function(token) {
            $window.localStorage.fbtoken = token;
        };

        $rootScope.getFacebookToken = function() {
            return $window.localStorage.fbtoken;
        }

        $rootScope.setProfileImage = function(image) {
            $window.localStorage.imagenPerfil = image;
        }

        $rootScope.getProfileImage = function() {
            return $window.localStorage.imagenPerfil;
        }

        /**
         ** Almacena en la memoria local del dispositivo un objeto que almacena los datos de la tarjeta
         ** @param tarjeta (Objeto) objeto con los datos de la tarjeta
         */
        $rootScope.setTarjetaLocalStorage = function(tarjeta) {
            $window.localStorage.tarjeta = JSON.stringify(tarjeta);
        }

        $rootScope.getTarjetaLocalStorage = function() {
            if ($window.localStorage.tarjeta) return JSON.parse($window.localStorage.tarjeta);
            return null;
        }

        $rootScope.isSessionActive = function () {
            console.log(typeof $window.localStorage.token);
            return $window.localStorage.token ? true : false;
        }

        $rootScope.clearLocalStorage = function() {
            $window.localStorage.clear();
        }

        return {
            signin: function (form) {
                return $http.post(base+'/api/v1/aenea/usuarios/login', form);
            },
            signup: function (form) {
                return $http.post(base+'/api/v1/aenea/usuarios/registra', form);
            },
            getUser : function(email) {
                return $http.get(base+'/api/v1/aenea/usuarios/usuario/' + email, {
                    method : 'GET',
                    headers: {
                        token: email
                    }
                });
            },
            updateProject : function(email, id, capital, donantes) {
                var datos = {
                    capitalRecaudado : capital,
                    numeroDonantes : donantes
                }
                return $http.put(base+'/api/v1/aenea/proyectos/proyecto/' + id, datos, {
                    method : 'PUT',
                    headers : {
                        token : email
                    }
                });
            },
            updateUser : function(user) {
                return $http.put(base+'/api/v1/aenea/usuarios/usuario/' + user.id, user, {
                    method: 'PUT',
                    headers: {
                        token: user.email
                    }
                });
            },
            getAll: function (email) {
                return $http.get(base+'/api/v1/aenea/proyectos/all', {
                    method: 'GET',
                    headers: {
                        token: email
                    }
                });
            },
            getOne: function (id, email) {
                return $http.get(base+'/api/v1/aenea/proyectos/proyecto/' + id, {
                    method: 'GET',
                    headers: {
                        token: email
                    }
                });
            },
            getImage : function(url,email) {
                return $http.get(base+'/api/v1/aenea/proyectos/imagen/'+url , {
                    method : 'GET',
                    headers : {
                        token : email
                    }
                });
            },
            getBase : function() {
                return base;
            }
        }
    });