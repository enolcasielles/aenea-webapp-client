
    servicios.factory('SHARE', function($cordovaSocialSharing) {

        var data = {};
        data.message = '';
        data.link = 'http://www.aenea.co';
        data.subject = '';

        return {
            
            moreShare : function() {
                return $cordovaSocialSharing.share(data.message, data.subject, null, data.link);
            },

            facebookShare : function() {
                return $cordovaSocialSharing.shareViaFacebook(data.message, null, data.link);
            },

            twitterShare : function() {
                return $cordovaSocialSharing.shareViaTwitter(data.message, null, data.link);
            },

            whatsappShare : function() {
                return $cordovaSocialSharing.shareViaWhatsApp(data.message, null, data.link);
            },

            mailShare : function() {
                return $cordovaSocialSharing.shareViaEmail(data.message, data.subject, null, null, null, null);
            },

            pinterestShare : function() {
                return $cordovaSocialSharing.shareViaPinterest(data.message, null);
            },

            setDatos : function(subject,link,message) {
                data.message = message;
                if (link != '') data.link = link;
                data.subject = subject;
            }
        }

    });
