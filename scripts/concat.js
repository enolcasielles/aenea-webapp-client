/**
 ** Script para generar los js unificados.
 */

/**
 * Script que unifica controladores, directivas y servicios en lso ficheros que se ditribuiran
 * en el html.
 *
 * ¿Como usarlo?
 * El script esta separado para la unificacion de controladores, servicios y directivas.
 * Incluir en cada apartado todos aquellos archivos que se quiera unificar y en el orden requerido.
 * Para realizar la tarea ejecutar "node scripts/concat.js" desde la raiz de la aplicacion
 */


  var concat = require('concat-files');
 
  
  //Unifico controladores
  var base = 'www/js/controllers/';
  concat([
    base + 'controllers.js',
    base + 'inicioCtrl.js',
    base + 'LoginCtrl.js',
    base + 'ProyectosCtrl.js',
    base + 'ProyectoCtrl.js',
    base + 'RegisterCtrl.js',
    base + 'PerfilCtrl.js',
  ], base + '../controllers.js', function() {
    console.log('done');
  });

  //Unifico directivas
  base = 'www/js/directives/';
  concat([
    base + 'directives.js',
    base + 'ionSearch.js',
    base + 'map.js',
    base + 'moveNextOnMaxlength.js',
    base + 'slideAlong.js'
  ], base + '../directives.js', function() {
    console.log('done');
  });

  //Unifico servicios
  base = 'www/js/services/';
  concat([
    base + 'services.js',
    base + 'API.js',
    base + 'USER.js',
    base + 'PROYECTOS.js',
    base + 'SHARE.js',
    base + 'STRINGS.js'
  ], base + '../services.js', function() {
    console.log('done');
  });